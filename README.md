## Heimdall
Bastille Template for [Heimdall](https://github.com/linuxserver/Heimdall) a self-hosted dashboard web application

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/sasaha5379/heimdall
```

## Usage
```shell
bastille template TARGET sasaha5379/heimdall
```

Note: tested with Heimdall version 2.6.1 on FreeBSD 14.0 release